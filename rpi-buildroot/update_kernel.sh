#!/bin/bash
git clone https://github.com/raspberrypi/tools
CCPREFIX=tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian/bin/arm-linux-gnueabihf-
wget -4 https://github.com/raspberrypi/linux/archive/rpi-4.2.y.zip
unzip rpi-4.2.y.zip
cd linux-rpi-4.2.y
ARCH=arm CROSS_COMPILE=${CCPREFIX} make bcmrpi_defconfig
ARCH=arm CROSS_COMPILE=${CCPREFIX} make zImage modules dtbs
sudo ARCH=arm CROSS_COMPILE=${CCPREFIX} make modules_install
 
 
#sudo cp arch/arm/boot/dts/*.dtb /boot/
#sudo cp arch/arm/boot/dts/overlays/*.dtb* /boot/overlays/
#sudo cp arch/arm/boot/dts/overlays/README /boot/overlays/
#sudo scripts/mkknlimg arch/arm/boot/zImage /boot/kernel.img